1) Setup/Tools
Install JDK 8
Install latest chrome web browser
install and setup latest Maven
Install latest LTS jenkins
install any java IDE that supports maven

2) Test execution
Login on Jenkins
From the jenkins dashboard, and a "New Item" by following the below steps

Click "New Item"
Set "Enter an item name" to "AnyBuildName"
Click "Freestyle project"
Click "Ok"

click "Add build step" and select "Execute Windows batch command"
Copy and Paste below line into the text area(If you like, you can replace "testZen" with your username and CurrentOrganization with your password) in the git clone URL below. The current username and passowrd will also work without being changed
git -C inspiredtesting pull || git clone https://testZen:CurrentOrganization@bitbucket.org/mxolisi/inspiredtesting.git inspiredtesting

Again, click "Add build step" and select "Execute Windows batch command"
Copy and Paste below line into the text area
mvn -f inspiredtesting/ clean install

Click "Apply" at the bottom of the screen
Click "Save" at the bottom of the screen
Click "Build Now"

After the job finishes building or running the tests
Click "Workspace"
Click "inspirediesting"
Click "resources"
Click "report"
click "(all files in zip)"

Once the report zip folder completes downloading
Open it and navigate to the HTML_Test_Report.html file which is a rich html report with

3) Test results/report
After you complete the steps in "Test execution", the script will output a rich html report which contains the steps taken by the tests and screen shots with the name "HTML_Test_Report.html"
The file will be located in the folder "InspiredTesting/resources"
Another report which can be understood by jenkins/bamboo will be produced under InspiredTesting/target/surefire-reports/junitreports

4) Test data
The json test data is located in InspiredTesting/resources/testdata/data.json
The test data for the rest of the tests is hard-coded in the script and can be updated in the File InspiredTesting/src/test/java/data/TestDataProvider.java or on the test method themselves.
All the test methods are located in the java class InspiredTesting/src/test/java/tests/task/TaskTest.java

5) Data Keywords
There are some String that have special meaning to the scripts when applied to selecting an option from a drop down list, these keys/string are SelectUsingIndex:0,All
The web page scripts can select information from a drop down using two ways
1. By index 
2. By using using the text that is displayed on the drop down option

"SelectUsingIndex:0" is 0 based, meaning when the script encounters "SelectUsingIndex:0" it will select the first option on the drop down in question, when the script encounters "SelectUsingIndex:1", it will select the second option in the drop down in question
for example, you can select the first customer using "SelectUsingIndex:1" or by typing the actual customer name Hermoine Granger 


On the account screen, you can instruct the script to transact with all the accounts using the Key "All"
You can also select the first account by using "SelectUsingIndex:0"

Using the keywords allows the script to work with the data on the application without needing to know the customer names in advance and know the respective accounts under each customer

6) Viewing the source code
You can open the inspiredtesting project folder with any Java IDE that supports maven
