/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.task;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.google.gson.Gson;
import data.TestDataProvider;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import lombok.Cleanup;
import network.Network;
import object.Address;
import object.Company;
import object.Geo;
import object.User;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AllPages;
import tests.Base;

/**
 *
 * @author mxolisi
 */
public class TaskTest extends Base {

    @Test
    public void test1() {
        ExtentTest extentTest = extent.createTest("Task 1 test 1");

        Geo geo = new Geo("-37.3159", "81.1496");
        Address address = new Address("Lulu street", "suit 222", "city 111", "zip 0101", geo);
        Company company = new Company("My company", "catch phrase for my company", "The bs");
        User user = new User(11, "Name of user", "Username of user", "testemail@mail.zo.za", address, "www.test.ord.za", company);

        String temp = new Gson().toJson(user);
        try {
            extentTest.log(Status.PASS, "Posting user <pre>" + temp + "</pre> to endpoint " + properties.getProperty("usersEnpoint"));
            Response response = new Network().postResource(properties.getProperty("usersEnpoint"), temp);

            int expectedResponseCode = 201;
            String message = "Verifying post status equals " + expectedResponseCode;
            Assert.assertEquals(response.getStatus(), expectedResponseCode);
            extentTest.log(Status.PASS, message);
        } catch (AssertionError e) {
            extentTest.log(Status.FAIL, "Something went wrong " + e.getMessage());
            throw e;
        }

    }

    @Test
    public void test2() {
        ExtentTest extentTest = extent.createTest("Task 1 test 2");
        int id = 2;
        testUserEndPoint(extentTest, id);
    }

    @Test
    public void test3() {
        ExtentTest extentTest = extent.createTest("Task 1 test 3");
        for (int i = 1; i <= 10; i++) {
            testUserEndPoint(extentTest, i);
        }
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "transactionData")
    public void test4(Map<String, String> data) throws Exception {
        ExtentTest extentTest = extent.createTest(data.get("testName"));
        AllPages allPages = null;
        @Cleanup("quit")
        WebDriver driver = null;

        try {
            driver = initialiseDriver(properties.getProperty("hideBrowser"));
            allPages = new AllPages(driver, extentTest);
            loginWithCustomer(driver, extentTest, properties.getProperty("url"), data.get("customerName"));

            if (data.get("account").equalsIgnoreCase("All")) {
                List<String> allAccounts = allPages.getCustomerAccountPage().getAllAccountDropDownOptions();

                for (String tempAccount : allAccounts) {
                    doTransaction(driver, extentTest, data.get("transactionType"), data.get("creditAmount"), tempAccount, data.get("expectedMessage"), data.get("verifyTransactionOnTransactionListTable"));
                    if (data.get("verifyTransactionOnTransactionListTable").equalsIgnoreCase("yes")) {
                        allPages.getTransactionListPage().clickBackBtn();
                        allPages.getCustomerAccountPage().waitForPageLoad(10);
                    }
                }
            } else {
                doTransaction(driver, extentTest, data.get("transactionType"), data.get("creditAmount"), data.get("account"), data.get("expectedMessage"), data.get("verifyTransactionOnTransactionListTable"));
            }

        } catch (Exception | AssertionError e) {
            if (allPages != null) {
                extentTest.log(Status.FAIL, e.getMessage(), allPages.getMainMenuPage().takeBase64SreenShot());
            } else {
                extentTest.log(Status.FAIL, e.getMessage());
            }
            throw e;
        }
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "transactionData")
    public void test5(Map<String, String> data) throws Exception {
        ExtentTest extentTest = extent.createTest(data.get("testName"));
        AllPages allPages = null;
        @Cleanup("quit")
        WebDriver driver = null;

        try {
            driver = initialiseDriver(properties.getProperty("hideBrowser"));
            allPages = new AllPages(driver, extentTest);
            loginWithCustomer(driver, extentTest, properties.getProperty("url"), data.get("customerName"));
            doTransaction(driver, extentTest, "Credit", data.get("creditAmount"), data.get("account"), data.get("expectedMessage"), data.get("verifyTransactionOnTransactionListTable"));
            allPages.getTransactionListPage().clickBackBtn();
            allPages.getCustomerAccountPage().waitForPageLoad(10);
            doTransaction(driver, extentTest, "Debit", data.get("debitAmount"), data.get("account"), data.get("expectedDebitMessage"), data.get("verifyTransactionOnTransactionListTable"));
        } catch (Exception | AssertionError e) {
            if (allPages != null) {
                extentTest.log(Status.FAIL, e.getMessage(), allPages.getMainMenuPage().takeBase64SreenShot());
            } else {
                extentTest.log(Status.FAIL, e.getMessage());
            }
            throw e;
        }
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "transactionData")
    public void test6(Map<String, String> data) throws Exception {
        data.put("testName", "Task 2 Test 4");
        data.put("expectedMessage", "Deposit Successful");
        data.put("expectedDebitMessage", "Transaction successful");
        data.put("verifyTransactionOnTransactionListTable", "Yes");
        test5(data);
    }

}
