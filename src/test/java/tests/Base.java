/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.google.gson.Gson;
import object.Transaction;
import helper.Util;
import io.github.bonigarcia.wdm.WebDriverManager;
import java.io.IOException;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import javax.ws.rs.core.Response;
import network.Network;
import object.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.AllPages;

/**
 *
 * @author mxolisi
 */
public class Base {

    protected ExtentReports extent;
    protected Properties properties;

    @BeforeSuite
    public void setup() throws IOException {
        WebDriverManager.chromedriver().setup();
        extent = new ExtentReports();
        ExtentSparkReporter spark = new ExtentSparkReporter("resources/report/HTML_Test_Report.html");
        extent.attachReporter(spark);
        properties = Util.getConfigProperties();
    }

    @AfterSuite
    public void tearDown() {
        extent.flush();
    }

    public void testUserEndPoint(ExtentTest extentTest, int id) {
        String endpoint = properties.getProperty("usersEnpoint") + id;

        String message = "Getting resource " + endpoint;
        extentTest.log(Status.PASS, message);

        Network network = new Network();
        Response response = network.getResource(endpoint);
        int expectedStatyusCode = 200;

        message = "Checking status code is " + expectedStatyusCode;
        Assert.assertEquals(response.getStatus(), expectedStatyusCode, message);
        extentTest.log(Status.PASS, message);
        String temp = response.readEntity(String.class);
        extentTest.log(Status.PASS, "Recieved user <pre>" + temp + "</pre>");

        message = "Checking User id is " + id;
        User user = new Gson().fromJson(temp, User.class);

        Assert.assertEquals(user.getId(), id, message);
        extentTest.log(Status.PASS, message);
    }

    public WebDriver initialiseDriver(String hideBrowser) {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");

        if (hideBrowser.equalsIgnoreCase("Yes")) {
            options.addArguments("--headless");
        }

        return new ChromeDriver(options);
    }

    public void loginWithCustomer(WebDriver driver, ExtentTest extentTest, String url, String customerName) {
        AllPages allPages = new AllPages(driver, extentTest);
        allPages.getLoginPage().customerLogin(url);
        allPages.getCustomerSelectionPage().selectCustomer(customerName);
        allPages.getCustomerAccountPage().waitForPageLoad(10);
    }

    public void doTransaction(WebDriver driver, ExtentTest extentTest, String transactionType, String transactionAmount, String account, String expectedMessage, String verifyTransactionOnTransactionListTable) throws ParseException {
        AllPages allPages = new AllPages(driver, extentTest);
        Transaction scriptTransaction = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy h:mm:ss a");

        scriptTransaction = allPages.getCustomerAccountPage().transact(transactionType, account, String.valueOf(transactionAmount));

        String message = "Verifying " + transactionType + " message is " + expectedMessage;
        Assert.assertEquals(scriptTransaction.getTransactionMessage(), expectedMessage, message);
        extentTest.log(Status.PASS, message);
        double expectedBalance;

        if (transactionType.equalsIgnoreCase("Credit")) {
            expectedBalance = Double.valueOf(scriptTransaction.getBalanceBefore()) + Double.valueOf(transactionAmount);
        } else {
            expectedBalance = Double.valueOf(scriptTransaction.getBalanceBefore()) - Double.valueOf(transactionAmount);
        }

        double actualDeposit = Double.valueOf(scriptTransaction.getBalanceAfter());

        message = "Verify balance was updated from " + scriptTransaction.getBalanceBefore() + " to " + expectedBalance + " after " + transactionType + " transaction";
        Assert.assertEquals(actualDeposit, expectedBalance, message);

        Assert.assertEquals(actualDeposit, expectedBalance, message);
        extentTest.log(Status.PASS, message);

        if (verifyTransactionOnTransactionListTable.equalsIgnoreCase("Yes")) {
            Transaction appTransaction = allPages.getTransactionListPage().getLastTransaction();

            int startResultTime = scriptTransaction.getStartTimeStamp().withNano(0).compareTo(appTransaction.getStartTimeStamp());
            message = "Verifying transaction time " + appTransaction.getStartTimeStamp().format(formatter) + " is equal or greater than " + scriptTransaction.getStartTimeStamp().format(formatter);
            Assert.assertTrue(startResultTime == 0 || startResultTime < 0, message);
            extentTest.log(Status.PASS, message);

            int endResultTime = scriptTransaction.getEndTimeStamp().withNano(0).compareTo(appTransaction.getStartTimeStamp());
            message = "Verifying transaction time " + appTransaction.getStartTimeStamp().format(formatter) + " is equal or less than " + scriptTransaction.getEndTimeStamp().format(formatter);
            Assert.assertTrue(endResultTime == 0 || endResultTime > 0, message);
            extentTest.log(Status.PASS, message);

            message = "Verifying " + transactionType + " amount of " + transactionAmount + " on the Transaction list";
            Assert.assertEquals(appTransaction.getAmount(), transactionAmount, message);
            extentTest.log(Status.PASS, message);

            message = "Verifying transaction type " + transactionType + " on the Transaction list";
            Assert.assertEquals(appTransaction.getTransactionType(), transactionType, message);
            extentTest.log(Status.PASS, message, allPages.getTransactionListPage().takeBase64SreenShot());
        }
    }

}
