/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.model.Media;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author mxolisi
 */
public class BasePage {

    protected WebDriver driver;
    protected ExtentTest extentTest;

    public BasePage(WebDriver driver, ExtentTest extentTest) {
        this.driver = driver;
        this.extentTest = extentTest;
    }

    public WebElement waitForElement(By lookUpForElementToWaitFor, int secondsToWait) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(secondsToWait));
        return wait.until(ExpectedConditions.elementToBeClickable(lookUpForElementToWaitFor));
    }

    public Media takeBase64SreenShot() {
        String base64Image = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
        return MediaEntityBuilder.createScreenCaptureFromBase64String(base64Image).build();
    }

    public void selectItem(By selectLookUp, String keyWord) {
        if (keyWord.contains("SelectUsingIndex:")) {
            int indexToSelect = Integer.valueOf(keyWord.replaceAll("SelectUsingIndex:", ""));
            selectItemByIndex(selectLookUp, indexToSelect);
        } else {
            selectItemByVisibleText(selectLookUp, keyWord);
        }
    }

    public void selectItemByVisibleText(By selectLookUp, String visibleText) {
        WebElement selectElement = waitForElement(selectLookUp, 10);
        Select select = new Select(selectElement);
        select.selectByVisibleText(visibleText);
    }

    public void selectItemByIndex(By selectLookUp, int index) {
        WebElement selectElement = waitForElement(selectLookUp, 10);
        Select select = new Select(selectElement);
        select.selectByIndex(index);
    }

    public List<String> getAllDropDownOptions(By selectLookUp) {
        List<String> allOptions = new ArrayList<>();
        Select select = new Select(driver.findElement(selectLookUp));
        List<WebElement> listOption = select.getOptions();
        for (int i = 0; i < listOption.size(); i++) {
            allOptions.add(listOption.get(i).getText());
        }
        return allOptions;
    }

    protected void waitForPageLoad(int waitSeconds) {
    }
}
