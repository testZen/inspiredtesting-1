/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import object.Transaction;
import java.time.LocalDateTime;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mxolisi
 */
public class CustomerAccountPage extends BasePage {

    private final By accoutSelect = By.id("accountSelect");
    private final By depositMenuBtn = By.xpath("//button[@ng-click='deposit()']");
    private final By withdrawMenuBtn = By.xpath("//button[@ng-click='withdrawl()']");
    private final By amountTxt = By.xpath("//input[@ng-model='amount']");
    private final By depositBtn = By.xpath("//button[text()='Deposit']");
    private final By withdrawBtn = By.xpath("//button[text()='Withdraw']");
    private final By transactionMessage = By.xpath("//span[@class='error ng-binding' and @ng-show='message']");
    private final By balanceLabel = By.xpath("//div[@ng-hide='noAccount'][1]//strong[2]");

    public CustomerAccountPage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    public Transaction transact(String transactionType, String account, String amount) {
        Transaction transaction = new Transaction();
        extentTest.log(Status.PASS, "Selecting Account " + account);
        selectItem(accoutSelect, account);
        String balanceBefore = getCurrentBalance();
        transaction.setBalanceBefore(balanceBefore);
        extentTest.log(Status.PASS, "Before Transaction Balance " + balanceBefore, takeBase64SreenShot());

        if (transactionType.equalsIgnoreCase("Credit")) {
            extentTest.log(Status.PASS, "Clicking Deposit menu option");
            driver.findElement(depositMenuBtn).click();
        } else {
            extentTest.log(Status.PASS, "Clicking Withdraw menu option");
            driver.findElement(withdrawMenuBtn).click();
        }

        WebElement amountTxtElement = waitForElement(amountTxt, 10);
        extentTest.log(Status.PASS, "Capturing amount " + amount);
        amountTxtElement.sendKeys(amount);

        transaction.setStartTimeStamp(LocalDateTime.now());

        if (transactionType.equalsIgnoreCase("Credit")) {
            extentTest.log(Status.PASS, "Clicking Deposit Button");
            driver.findElement(depositBtn).click();
        } else {
            extentTest.log(Status.PASS, "Clicking Withdraw Button");
            driver.findElement(withdrawBtn).click();
        }

        WebElement transactionMessageElement = waitForElement(transactionMessage, 10);
        transaction.setEndTimeStamp(LocalDateTime.now());
        transaction.setTransactionMessage(transactionMessageElement.getText());
        String balanceAfter = getCurrentBalance();
        transaction.setBalanceAfter(balanceAfter);
        extentTest.log(Status.PASS, "After Transaction Balance " + balanceAfter, takeBase64SreenShot());
        return transaction;
    }

    public List<String> getAllAccountDropDownOptions() {
        return getAllDropDownOptions(accoutSelect);
    }

    @Override
    public void waitForPageLoad(int waitSeconds) {
        waitForElement(accoutSelect, waitSeconds);
    }

    public String getCurrentBalance() {
        return driver.findElement(balanceLabel).getText();
    }

}
