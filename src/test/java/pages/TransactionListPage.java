/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import object.Transaction;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mxolisi
 */
public class TransactionListPage extends BasePage {

    private final By transactionsMenuBtn = By.xpath("//button[@ng-click='transactions()']");
    private final By transactionListTable = By.xpath("//table[@class='table table-bordered table-striped']");
    private final By transactionListTableDateTimeheader = By.partialLinkText("Date-Time");
    private final By transactionBackBtn = By.xpath("//button[text()='Back']");

    public TransactionListPage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    public Transaction getLastTransaction() throws ParseException {
        goToTransactionListPage();
        Transaction transaction;
        try {
            transaction = getTransactionFromAppTable();
        } catch (NoSuchElementException e) {
            extentTest.log(Status.INFO, "Transaction List page did not load transactions, will refreshing page", takeBase64SreenShot());
            driver.navigate().back();
            waitForPageLoad(10);
            driver.navigate().refresh();
            waitForPageLoad(10);
            transaction = getTransactionFromAppTable();
        }
        return transaction;
    }

    public void goToTransactionListPage() {
        waitForElement(transactionsMenuBtn, 10).click();
        waitForElement(transactionListTable, 10);
    }

    public void clickBackBtn() {
        extentTest.log(Status.PASS, "Clicking Transaction List page back button");
        driver.findElement(transactionBackBtn).click();
    }

    private Transaction getTransactionFromAppTable() throws ParseException {
        Transaction transaction = new Transaction();

        waitForPageLoad(10);
        WebElement transactionListTableElement = waitForElement(transactionListTable, 10);

        driver.findElement(transactionListTableDateTimeheader).click();
        waitForPageLoad(10);
        WebElement lastTableRow = transactionListTableElement.findElement(By.xpath("tbody/tr[1]"));
        List<WebElement> tds = lastTableRow.findElements(By.xpath("td"));

        String dateTime = tds.get(0).getText();
        String amout = tds.get(1).getText();
        String transactionType = tds.get(2).getText();

        transaction.setStartTimeStamp(LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern("MMM dd, yyyy h:mm:ss a")));
        transaction.setAmount(amout);
        transaction.setTransactionType(transactionType);

        return transaction;
    }

    @Override
    public void waitForPageLoad(int waitSeconds) {
        waitForElement(transactionListTable, 10);
    }

}
