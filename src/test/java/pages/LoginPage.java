/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author mxolisi
 */
public class LoginPage extends BasePage {

    private final By customerLoginBtn = By.xpath("//button[text()='Customer Login']");

    public LoginPage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    public void customerLogin(String url) {
        driver.get(url);
        extentTest.log(Status.PASS, "Navigated to URL " + url);
        driver.manage().window().maximize();
        waitForElement(customerLoginBtn, 15);
        driver.findElement(customerLoginBtn).click();
        extentTest.log(Status.PASS, "Clicked Login as customer");
    }
}
