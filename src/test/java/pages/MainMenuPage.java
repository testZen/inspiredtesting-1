/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author mxolisi
 */
public class MainMenuPage extends BasePage {

    private final By logoutBtn = By.xpath("//button[text()='Logout']");

    public MainMenuPage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    public void logOut() {
        driver.findElement(logoutBtn).click();
    }
}
