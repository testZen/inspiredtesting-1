/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import lombok.Getter;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author mxolisi
 */
@Getter
public class AllPages {

    private final LoginPage loginPage;
    private final CustomerSelectionPage customerSelectionPage;
    private final CustomerAccountPage customerAccountPage;
    private final MainMenuPage mainMenuPage;
    private final TransactionListPage transactionListPage;

    public AllPages(WebDriver driver, ExtentTest extentTest) {
        loginPage = new LoginPage(driver, extentTest);
        customerSelectionPage = new CustomerSelectionPage(driver, extentTest);
        customerAccountPage = new CustomerAccountPage(driver, extentTest);
        mainMenuPage = new MainMenuPage(driver, extentTest);
        transactionListPage = new TransactionListPage(driver, extentTest);
    }
}
