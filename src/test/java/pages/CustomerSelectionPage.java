/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author mxolisi
 */
public class CustomerSelectionPage extends BasePage {

    private final By customerSelect = By.id("userSelect");
    private final By loginBtn = By.xpath("//button[text()='Login']");

    public CustomerSelectionPage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    public void selectCustomer(String name) {
        extentTest.log(Status.PASS, "Selecting Customer " + name);
        selectItem(customerSelect, name);
        driver.findElement(loginBtn).click();
    }

}
