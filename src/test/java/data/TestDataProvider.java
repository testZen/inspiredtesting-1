/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import helper.Util;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.annotations.DataProvider;

/**
 *
 * @author mxolisi
 */
public class TestDataProvider {

    @DataProvider(name = "transactionData")
    public Object[][] transactionData(Method method) throws FileNotFoundException {

        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map;

        if (method.getName().equalsIgnoreCase("test4")) {
            map = new HashMap<>();
            map.put("testName", "Task 2 Test 1");
            map.put("customerName", "SelectUsingIndex:1");
            map.put("account", "SelectUsingIndex:0");
            map.put("creditAmount", "1500");
            map.put("expectedMessage", "Deposit Successful");
            map.put("transactionType", "Credit");
            map.put("verifyTransactionOnTransactionListTable", "No");
            list.add(map);

            map = new HashMap<>();
            map.put("testName", "Task 2 Test 2");
            map.put("customerName", "Neville Longbottom");
            map.put("account", "All");
            map.put("creditAmount", "1500");
            map.put("expectedMessage", "Deposit Successful");
            map.put("transactionType", "Credit");//debit ==> Transaction successful
            map.put("verifyTransactionOnTransactionListTable", "No");
            list.add(map);
        } else if (method.getName().equalsIgnoreCase("test5")) {
            map = new HashMap<>();
            map.put("testName", "Task 2 Test 3");
            map.put("customerName", "SelectUsingIndex:1");
            map.put("account", "SelectUsingIndex:0");
            map.put("creditAmount", "31459");
            map.put("expectedMessage", "Deposit Successful");
            map.put("debitAmount", "31459");
            map.put("expectedDebitMessage", "Transaction successful");
            map.put("transactionType", "Credit");
            map.put("verifyTransactionOnTransactionListTable", "Yes");
            list.add(map);
        } else if (method.getName().equalsIgnoreCase("test6")) {
            list = Util.getJsonData("resources/testdata/data.json");
        }

        Object[][] data = new Object[list.size()][1];

        for (int i = 0; i < list.size(); i++) {
            data[i][0] = list.get(i);
        }
        return data;
    }
}
