/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import com.google.gson.Gson;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author mxolisi
 */
public class Util {

    public static Properties getConfigProperties() throws IOException {
        InputStream input = new FileInputStream("resources/config.properties");
        Properties prop = new Properties();
        prop.load(input);
        return prop;
    }

    public static List<Map<String, String>> getJsonData(String fullFilePath) throws FileNotFoundException {
        List<Map<String, String>> data = null;
        Map<String, String>[] p = new Gson().fromJson(new FileReader(fullFilePath), Map[].class);
        data = Arrays.asList(p);
        return data;
    }

}
