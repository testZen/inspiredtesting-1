/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author mxolisi
 */
public class Network {

    public Response getResource(String uri) {
        Client client = ClientBuilder.newClient();
        return client.target(uri)
                .request(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response postResource(String uri, String str) {
        Client client = ClientBuilder.newClient();
        return client.target(uri)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(str, MediaType.APPLICATION_JSON));
    }
}
