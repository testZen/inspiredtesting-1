/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author mxolisi
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    
    private int id;
    private String name;
    private String username;
    private String email;
    private Address address;
    private String website;
    private Company company;
}
