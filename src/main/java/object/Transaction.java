/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author mxolisi
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    private LocalDateTime startTimeStamp;
    private LocalDateTime endTimeStamp;
    private String amount;
    private String balanceBefore;
    private String balanceAfter;
    private String transactionType;
    private String transactionMessage;
}
